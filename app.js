var irc = require('irc');
var util = require('util');
var config = require('./local_config.json');

var client = new irc.Client(config['server'], config["nick"], {
                             realName: config['real_name'],
                             port: config['port'],
                             secure: true,
                             ignoreSSLErrors: true,
                             selfSigned: true,
                             autoRejoin: true,
                             autoConnect: true,
                             certExpired: true,
                             stripColors: true,
                             showErrors: true,
                             debug: true,
                             password: config["server_password"],
                             channels: config["rooms"]});

function getDestination(nick, to) {
  if(to === config["nick"]) {
    return nick;
  }
  else {
    return to;
  }
}

function genRolls(count, dim) {
  var toReturn = [];
  for(var i = 0; i < count; i++) {
    toReturn.push(Math.floor(1 + Math.random() * dim));
  }
  return toReturn;
}

function doRolls(words) {
  var toReturn = "Rolls: ";
  for(var i = 0; i < words.length; i++) {
    var toCheck = words[i].trim();
    var sides = toCheck.split('d');
    if(sides.length != 2) {
      continue;
    }
    var count = parseInt(sides[0]);
    var dim = parseInt(sides[1]);
    if(isNaN(count) ||isNaN(dim)) {
      continue;
    }
    var rolls = genRolls(count, dim);
    var sum = rolls.reduce(function(a, b) { return a + b });
    toReturn += toCheck + ": [";
    for(var j = 0; j < rolls.length; j++) {
      toReturn += rolls[j];
      if(j < rolls.length -1) {
        toReturn += ", ";
      }
    }
    toReturn += "] - total: " + sum;
    if(i < words.length - 1) {
      toReturn += " || ";
    }
  }
  return toReturn;
}

function doHelp(words) {
  return "!roll <sets of die> - e.g. !roll 3d6 4d12 2d100";
}

function processCommand(text) {
  var words = text.trim().split(' ');
  if(words === undefined || words.length === 0) {
    return undefined;
  }
  switch(words[0]) {
    case '!roll':
      return doRolls(words.slice(1));
    case '!help':
      return doHelp(words.slice(1));
  }
}

client.addListener('message', function(nick, to, text, messageObj) {
  console.log(JSON.stringify(messageObj));
  var response = processCommand(text);
  if(response !== undefined) {
    client.say(getDestination(nick, to), response);
  }
});